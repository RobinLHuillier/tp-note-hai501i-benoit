package fr.umfds.agl.table;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Person {
    private int id;
    private String firstname;
    private String lastname;

    public Person(JsonNode datas) {
        this.id = datas.get("id").asInt();
        this.firstname = datas.get("firstname").asText();
        this.lastname = datas.get("lastname").asText();
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }
    
    public String toString() {
        return "[" + getId() + "] " + firstname + " " + lastname;
    }

    public ObjectNode serialize(ObjectMapper mapper) {
        ObjectNode node = mapper.createObjectNode();

        node.put("id", id);
        node.put("firstname", firstname);
        node.put("lastname", lastname);

        return node;
    }
}